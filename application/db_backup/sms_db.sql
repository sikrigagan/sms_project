-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 15, 2015 at 11:43 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sms_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `stu_record`
--

CREATE TABLE IF NOT EXISTS `stu_record` (
  `stu_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(250) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `yop` year(4) NOT NULL,
  `interest` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stu_record`
--

INSERT INTO `stu_record` (`stu_id`, `name`, `address`, `sex`, `yop`, `interest`) VALUES
(1, 'Gagan Sikri', '#322/1, Kaithal', 'Male', 2015, 'cricket'),
(14, 'Gaurav Gupta', 'SquareBoar, Gurgaon', 'Male', 2010, 'Programming'),
(16, 'Gaurav Sikri', 'Huda, Kaithal', 'Male', 2014, 'Arts'),
(19, 'Subodh', 'Kurkshetra University', 'Male', 2015, 'Sports');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `stu_record`
--
ALTER TABLE `stu_record`
  ADD PRIMARY KEY (`stu_id`) COMMENT 'primary key';

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `stu_record`
--
ALTER TABLE `stu_record`
  MODIFY `stu_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
