<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//I made only one controller(Student_Controller.php) for all operations.
class Student_Controller extends CI_Controller{

	//constructor to initialize model for getting student list on first view(students.php)
	public function __construct(){
		parent:: __construct();
		//sturd can be used instead of model_getstudent for calling functions of the model.
		$this->load->model('model_getstudent', 'sturd');
	}

	//index function(List all Students)
	public function index()
	{
	$data['row']=$this->sturd->fetch_student(); //fetch_student() is function in model for fetching student records.
	$this->load->view('students', $data);
	}

	//add student function.
	public function add()
	{
	$this->load->view('view_studentadd');
	}

	//db query to save record in database.
	function save(){
		$data=array(
			'name'=>$this->input->post('name'),
			'address'=>$this->input->post('address'),
			'sex'=>$this->input->post('sex'),
			'yop'=>$this->input->post('yop'),
			'interest'=>$this->input->post('interest'));
		$this->db->insert('stu_record',$data);
		redirect('Student_Controller/');
	}

	//Record edit function.
	public function edit($stu_id){
	$this->load->model('model_editstudent'); //model_editstudent have function get_student_row which require stu_id as parameter.
	$data['r']=$this->model_editstudent	->get_student_row($stu_id); //data is loaded to array $data[].
	$this->load->view('view_editstudent', $data);
	}

	#Update query function along with db query for updation of record.
	function update_stu(){
		$stu_id=$this->input->post('stu_id');
		$data=array(
			'name'=>$this->input->post('name'),
			'address'=>$this->input->post('address'),
			'sex'=>$this->input->post('sex'),
			'yop'=>$this->input->post('yop'),
			'interest'=>$this->input->post('interest'));
		$this->db->where('stu_id',$stu_id);
		$this->db->update('stu_record',$data);
		redirect('Student_Controller/');
	}

	//Delete query function.
	public function delete($stu_id)
	{
	$this->db->where('stu_id', $stu_id);
	$this->db->delete('stu_record');
	redirect('Student_Controller/');
	}
}
?>