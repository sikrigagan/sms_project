<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_EditStudent extends CI_Model{
	
	function get_student_row($stu_id){
	$this->db->where('stu_id',$stu_id);
	$query=$this->db->get('stu_record');
	//will return only one record(row)
	return $query->row();
	}
}
