<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_GetStudent extends CI_Model{
	
	public function fetch_student()
	{
		//query to fetch all records from db table.
		$query=$this->db->get('stu_record'); //stu_record is name of table.
		return $query->result(); //result from query is output which is displayed on view via controller.
	}
}