<html>
<head>
<title>Add New Student</title>
</head>
<body>
<div>
<span style="font-size: 26pt"><b>BTE Engineering College</b></span>&nbsp&nbsp &nbsp<a href="<?php echo site_url('student_controller/add'); ?>">Add Student</a>&nbsp | <a href="<?php echo site_url('student_controller/'); ?>">List all Students</a>
</div><br><hr><h2>Add Student Record</h2><br>
<div>
<form method="POST" action='<?php echo site_url("student_controller/save"); ?>'>
<b>Name of the Student:</b><br>
<input type="text" Name="name"><br><br>
<b>Address:</b><br>
<textarea Name="address" rows="5" cols="22"></textarea><br><br>
<b>Gender:</b><br>
<input type="radio" name="sex" value="Male" checked> Male<br>
<input type="radio" name="sex" value="Female"> Female<br><br>
<b>Expected Year of Passing:</b><br>
<select name="yop">
<option value="0">Select Year</option>
<option value="2010">2010</option>
<option value="2011">2011</option>
<option value="2012">2012</option>
<option value="2013">2013</option>
<option value="2014">2014</option>
<option value="2015">2015</option>
</select><br><br>
<b>Extra Curricular Interests:</b><br>
<input type="radio" name="interest" value="Sports"> Sports<br>
<input type="radio" name="interest" value="Programming"> Programming<br>
<input type="radio" name="interest" value="Arts"> Arts<br>
<input type="radio" name="interest" value="Music"> Music<br>
<br><input type="submit" value="Add"> &nbsp 
<a href='<?php echo site_url('student_controller/'); ?>'>Cancel</a>
</form>
</body>
</html>
