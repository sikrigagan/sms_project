<html>
<head>
<title>Edit Student Record</title>
</head>
<body>
<div>
<span style="font-size: 26pt"><b>BTE Engineering College</b></span>&nbsp&nbsp &nbsp<a href="<?php echo site_url('student_controller/add'); ?>">Add Student</a>&nbsp | <a href="<?php echo site_url('student_controller/'); ?>">List all Students</a>
</div><br><hr><h2>Edit Student Record</h2><br>
<div>
<form method="POST" action='<?php echo site_url("Student_Controller/update_stu"); ?>'>
<b>Student ID:</b><br>
<input type="text" Name="stu_id" readonly value="<?php echo $r->stu_id ?>"><br><br>
<b>Name of the Student:</b><br>
<input type="text" Name="name" value="<?php echo $r->name ?>"><br><br>
<b>Address:</b><br>
<textarea name="address" rows="5" cols="22" ><?php echo $r->address ?></textarea><br><br>
<b>Gender:</b><br>
<input type="radio" name="sex" value="Male" <?php if($r->sex=='Male') echo "checked"; ?>>Male<br>
<input type="radio" name="sex" value="Female" <?php if($r->sex=='Female') echo "checked"; ?>>Female<br><br>
<b>Expected Year of Passing:</b><br>
<select name="yop">
<option value="0">Select Year</option>
<option value="2010" <?php if($r->yop=="2010"){echo "selected=selected";}?>>2010</option>
<option value="2011" <?php if($r->yop=="2011"){echo "selected=selected";}?>>2011</option>
<option value="2012" <?php if($r->yop=="2012"){echo "selected=selected";}?>>2012</option>
<option value="2013" <?php if($r->yop=="2013"){echo "selected=selected";}?>>2013</option>
<option value="2014" <?php if($r->yop=="2014"){echo "selected=selected";}?>>2014</option>
<option value="2015" <?php if($r->yop=="2015"){echo "selected=selected";}?>>2015</option>
</select>
<br><br>
<b>Extra Curricular Interests:</b><br>
<input type="radio" name="interest" value="Sports" <?php if($r->interest=='Sports') echo "checked"; ?>>Sports<br>
<input type="radio" name="interest" value="Programming" <?php if($r->sex=='Programming') echo "checked"; ?>>Programming<br>
<input type="radio" name="interest" value="Arts" <?php if($r->interest=='Arts') echo "checked"; ?>>Arts<br>
<input type="radio" name="interest" value="Music" <?php if($r->interest=='Music') echo "checked"; ?>>Music<br>
<br>
<input type="submit" value="Update"> &nbsp 
<a href='<?php echo site_url('student_controller/'); ?>'>Cancel</a>
</form>
</body>
</html>
